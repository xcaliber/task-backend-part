<?php

namespace Api\Action;

use Api\Entity\User;
use Doctrine\ORM\EntityRepository;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class GetUserAction implements ServerMiddlewareInterface
{
    /**
     * @var EntityRepository
     */
    private $userRepository;

    /**
     * GetUserAction constructor.
     * @param EntityRepository $userRepository
     */
    public function __construct(EntityRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse|ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $id = $request->getAttribute('id');

        $transaction = $this->userRepository->find($id);

        if ($transaction) {
            return new JsonResponse($transaction->getArrayCopy());
        } else {
            return new JsonResponse([], 404);
        }
    }


}