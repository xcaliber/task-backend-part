<?php

namespace Api\Action;

use Api\Entity\Transaction;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

final class GetTransactionsFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return GetTransactionsAction|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);
        /** @var EntityRepository $repository */
        $repository = $em->getRepository(Transaction::class);

        return new GetTransactionsAction($repository);
    }

}