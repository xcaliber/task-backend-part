<?php

namespace Api\Action;

use Api\Service\PaymentMethodService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

final class GetPaymentMethodsFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return GetPaymentMethods|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new GetPaymentMethods(
            new PaymentMethodService()
        );
    }

}