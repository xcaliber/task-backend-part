<?php

namespace Api\Action;

use Api\Entity\Transaction;
use Api\Entity\User;
use Api\Repository\TransactionRepository;
use Api\Repository\UserRepository;
use Api\Service\TransactionService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class CreateTransactionFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CreateTransactionAction|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);

        /** @var TransactionRepository $transactionRepository */
        $transactionRepository = $em->getRepository(Transaction::class);

        /** @var UserRepository $userRepository */
        $userRepository = $em->getRepository(User::class);

        /** @var InputFilterPluginManager $inputFilters */
        $inputFilters = $container->get('InputFilterManager');

        /** @var InputFilterInterface $inputFilter */
        $inputFilter = $inputFilters->get('Api\\Validator\\Transaction');

        return new CreateTransactionAction(
            new TransactionService($transactionRepository, $userRepository),
            $inputFilter
        );
    }
}

