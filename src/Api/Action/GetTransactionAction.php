<?php

namespace Api\Action;

use Doctrine\ORM\EntityRepository;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

final class GetTransactionAction implements ServerMiddlewareInterface
{
    /**
     * @var EntityRepository
     */
    private $transactionRepository;

    /**
     * GetTransactionsAction constructor.
     * @param EntityRepository $transactionRepository
     */
    public function __construct(EntityRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse|ResponseInterface
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $id = $request->getAttribute('id');

        $transaction = $this->transactionRepository->find($id);

        if ($transaction) {
            return new JsonResponse($transaction->getArrayCopy());
        } else {
            return new JsonResponse([], 404);
        }
    }
}

