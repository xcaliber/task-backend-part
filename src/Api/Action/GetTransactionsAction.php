<?php

namespace Api\Action;

use Api\Entity\Transaction;
use Doctrine\ORM\EntityRepository;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

final class GetTransactionsAction implements ServerMiddlewareInterface
{
    /**
     * @var EntityRepository
     */
    private $transactionRepository;

    /**
     * GetTransactionsAction constructor.
     * @param EntityRepository $transactionRepository
     */
    public function __construct(EntityRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return ResponseInterface|JsonResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $queryParams = $request->getQueryParams();

        $filters = isset($queryParams['filters']) ? $queryParams['filters'] : [];
        $limit = isset($queryParams['limit']) ? $queryParams['limit'] : 10;
        $page = isset($queryParams['page']) ? $queryParams['page'] : 0;
        $order = isset($queryParams['order']) ? $queryParams['order'] : ['id' => 'ASC'];

        return new JsonResponse([
            'pagination' => [
                'limit' => $limit,
                'page' => $page,
                'order' => $order,
            ],
            'data' => array_map(function (Transaction $transaction) {
                return $transaction->getArrayCopy();
            }, $this->transactionRepository->findBy($filters, $order, $limit, $page))
        ]);
    }
}
