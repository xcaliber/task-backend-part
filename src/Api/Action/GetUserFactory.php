<?php

namespace Api\Action;

use Api\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class GetUserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);
        /** @var EntityRepository $repository */
        $repository = $em->getRepository(User::class);

        return new GetUserAction($repository);
    }

}