<?php

namespace Api\Action;

use Api\Service\TransactionService;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;
use Zend\InputFilter\InputFilterInterface;

final class CreateTransactionAction implements ServerMiddlewareInterface
{
    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * @var InputFilterInterface
     */
    private $inputFilter;

    /**
     * CreateTransactionAction constructor.
     * @param TransactionService $transactionService
     * @param InputFilterInterface $inputFilter
     */
    public function __construct(TransactionService $transactionService, InputFilterInterface $inputFilter)
    {
        $this->transactionService = $transactionService;
        $this->inputFilter = $inputFilter;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return ResponseInterface|JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $data = $request->getParsedBody();
        $this->inputFilter->setData($data);

        if ($this->inputFilter->isValid()) {
            $data = $this->inputFilter->getValues();

            $transaction = $this->transactionService->create($data);
            $transaction = $this->transactionService->process($transaction);

            return new JsonResponse($transaction->getArrayCopy());
        } else {
            return new JsonResponse(['validation_messages' => $this->inputFilter->getMessages()], 422);
        }
    }
}

