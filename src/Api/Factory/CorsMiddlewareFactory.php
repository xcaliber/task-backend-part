<?php

namespace Api\Factory;

use Tuupola\Middleware\CorsMiddleware;
use Zend\Diactoros\Response;
use Zend\Stratigility\Middleware\DoublePassMiddlewareDecorator;

class CorsMiddlewareFactory
{
    /**
     * @param $container
     * @return DoublePassMiddlewareDecorator
     */
    public function __invoke($container)
    {
        return new DoublePassMiddlewareDecorator(
            new CorsMiddleware([
                "origin" => ["*"],
                "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
                "headers.allow" => ["Content-Type", "Accept"],
                "headers.expose" => [],
                "credentials" => false,
                "cache" => 0,
            ]),
            new Response()
        );
    }
}
