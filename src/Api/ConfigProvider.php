<?php

namespace Api;

use Tuupola\Middleware\CorsMiddleware;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'factories'  => [
                Action\PingAction::class => Action\PingFactory::class,
                Action\GetTransactionsAction::class => Action\GetTransactionsFactory::class,
                Action\GetTransactionAction::class => Action\GetTransactionFactory::class,
                Action\CreateTransactionAction::class => Action\CreateTransactionFactory::class,
                Action\GetPaymentMethods::class => Action\GetPaymentMethodsFactory::class,
                Action\GetUserAction::class => Action\GetUserFactory::class,
                CorsMiddleware::class => Factory\CorsMiddlewareFactory::class,
            ],
        ];
    }
}
