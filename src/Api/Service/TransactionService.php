<?php

namespace Api\Service;

use Api\Entity\Transaction;
use Api\Entity\User;
use Api\Repository\TransactionRepository;
use Api\Repository\UserRepository;

class TransactionService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * TransactionService constructor.
     * @param TransactionRepository $transactionRepository
     * @param UserRepository $userRepository
     */
    public function __construct(TransactionRepository $transactionRepository, UserRepository $userRepository)
    {
        $this->transactionRepository = $transactionRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $data
     * @return Transaction
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(array $data): Transaction
    {
        $user = $this->userRepository->findOneBy(['email' => $data['user']['email']]);

        if (!$user) {
            $user = new User();
        }

        $user->exchangeArray($data['user']);
        $this->userRepository->save($user);

        $data['user'] = $user;

        $transaction = new Transaction();
        $transaction->exchangeArray($data);

        $this->transactionRepository->save($transaction);

        return $transaction;
    }

    /**
     * @param Transaction $transaction
     * @return Transaction
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function process(Transaction $transaction): Transaction
    {
        /**
         * This simulates transaction processing
         */
        sleep(2);

        $statuses = ['success', 'failed', 'error', 'cancelled'];

        $transaction->setEndTime(new \DateTime());

        if (!$transaction->getAttributes()) {
            $transaction->setStatus('cancelled');
        } else {
            $transaction->setStatus($statuses[rand(0, count($statuses) - 1)]);
        }

        $this->transactionRepository->save($transaction);

        return $transaction;
    }

}