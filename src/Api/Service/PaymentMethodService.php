<?php

namespace Api\Service;

class PaymentMethodService
{
    /**
     * @return array
     */
    final public function getList(): array
    {
        return [
            $this->getPayPal(),
            $this->getCashlib(),
            $this->getVisa(),
            $this->getEntercash()
        ];
    }

    /**
     * @return array
     */
    private function getPayPal(): array
    {
        return [
            'name' => 'paypal',
            'label' => 'PayPal',
            'types' => [
                'deposit',
                'withdraw'
            ],
            'additionalData' => [
                [
                    'name' => 'username',
                    'label' => 'User name',
                    'type' => 'text',
                    'required' => true
                ],
                [
                    'name' => 'password',
                    'label' => 'Password',
                    'type' => 'password',
                    'required' => true
                ]
            ],
        ];
    }

    /**
     * @return array
     */
    private function getCashlib(): array
    {
        return [
            'name' => 'cashlib',
            'label' => 'Cashlib',
            'types' => [
                'deposit',
            ],
            'additionalData' => [
                [
                    'name' => 'voucher_code',
                    'label' => 'Voucher Code',
                    'type' => 'text',
                    'required' => true
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function getVisa(): array
    {
        return [
            'name' => 'visa',
            'label' => 'VISA',
            'types' => [
                'deposit',
                'withdraw'
            ],
            'additionalData' => [
                [
                    'name' => 'card_number',
                    'label' => 'card_number',
                    'type' => 'text',
                    'required' => true
                ],
                [
                    'name' => 'cvv',
                    'label' => 'CVV',
                    'type' => 'text',
                    'required' => true
                ],
                [
                    'name' => 'cardholder',
                    'label' => 'Card Holder Name',
                    'type' => 'text',
                    'required' => true
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    private function getEntercash(): array
    {
        return [
            'name' => 'entercash',
            'label' => 'Bank Withdrawals',
            'types' => [
                'withdraw'
            ],
            'additionalData' => [
                [
                    'name' => 'iban',
                    'label' => 'IBAN',
                    'type' => 'text',
                    'required' => true
                ],
                [
                    'name' => 'bic',
                    'label' => 'Swift/BIN',
                    'type' => 'text',
                    'required' => true
                ],
            ]
        ];
    }
}