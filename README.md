Paypi Exercise
==============

### Requirements:
* minimum php 7.0
* php pdo sqlite driver - php-sqlite3

### How To start

In directory where the project was cloned run following CLI commands.

Initialize code:

```php composer.phar install```

Start local php server

```php composer.phar serve```

Initialize database:

```php composer.phar db-schema-create``` 

Output should look like this:
```bash
> touch data/db/local.sqlite && chmod 0644 data/db/local.sqlite && php vendor/bin/doctrine orm:schema-tool:create
ATTENTION: This operation should not be executed in a production environment.

Creating database schema...
Database schema created successfully!
```

```php composer.phar generate-proxies``` 

Output should look like this:
```bash
> php vendor/bin/doctrine orm:generate-proxies
Processing entity "Api\Entity\User"
Processing entity "Api\Entity\Transaction"

Proxy classes generated to "<username>/paypi-exersise-backend/data/cache/DoctrineEntityProxy"
```

### Api endpoints:

#### Ping

Returns current time

###### Request 
``` GET http://127.0.0.1:8181/api/ping```

###### Response
```json
{
    "ack": 1539957138
}
```

#### Get available payment methods

Gets list of all available payment methods with addition data that should be provided (otherwise transaction status would be null). 

###### Request 
``` GET 127.0.0.1:8181/api/methods```

###### Response
```json
[
    {
        "name": "paypal",
        "label": "PayPal",
        "types": [
            "deposit",
            "withdraw"
        ],
        "additionalData": [
            {
                "name": "username",
                "label": "User name",
                "type": "text",
                "required": true
            },
            {
                "name": "password",
                "label": "Password",
                "type": "password",
                "required": true
            }
        ]
    },
    {
        "name": "cashlib",
        "label": "Cashlib",
        "types": [
            "deposit"
        ],
        "additionalData": [
            {
                "name": "voucher_code",
                "label": "Voucher Code",
                "type": "text",
                "required": true
            }
        ]
    },
    {
        "name": "visa",
        "label": "VISA",
        "types": [
            "deposit",
            "withdraw"
        ],
        "additionalData": [
            {
                "name": "card_number",
                "label": "card_number",
                "type": "text",
                "required": true
            },
            {
                "name": "cvv",
                "label": "CVV",
                "type": "text",
                "required": true
            },
            {
                "name": "cardholder",
                "label": "Card Holder Name",
                "type": "text",
                "required": true
            }
        ]
    },
    {
        "name": "entercash",
        "label": "Bank Withdrawals",
        "types": [
            "withdraw"
        ],
        "additionalData": [
            {
                "name": "iban",
                "label": "IBAN",
                "type": "text",
                "required": true
            },
            {
                "name": "bic",
                "label": "Swift/BIN",
                "type": "text",
                "required": true
            }
        ]
    }
]
```

#### Create transaction

Creates new transaction for allowed payment method.
For this endpoint exists user validation. Example validation error response for empty ```{}``` is:

```json
{
    "validation_messages": {
        "provider": {
            "isEmpty": "Value is required and can't be empty"
        },
        "user": {
            "first_name": {
                "isEmpty": "Value is required and can't be empty"
            },
            "last_name": {
                "isEmpty": "Value is required and can't be empty"
            },
            "gender": {
                "isEmpty": "Value is required and can't be empty"
            },
            "email": {
                "isEmpty": "Value is required and can't be empty"
            },
            "address": {
                "isEmpty": "Value is required and can't be empty"
            },
            "city": {
                "isEmpty": "Value is required and can't be empty"
            },
            "zip": {
                "isEmpty": "Value is required and can't be empty"
            },
            "country_code": {
                "isEmpty": "Value is required and can't be empty"
            },
            "birthday": {
                "isEmpty": "Value is required and can't be empty"
            }
        },
        "type": {
            "isEmpty": "Value is required and can't be empty"
        },
        "amount": {
            "isEmpty": "Value is required and can't be empty"
        },
        "currency": {
            "isEmpty": "Value is required and can't be empty"
        }
    }
}
```

###### Request 
``` POST 127.0.0.1:8181/api/transactions```

JSON body:
```json
{
	"provider" : "paypal",
	"type" : "deposit",
	"amount" : 100,
	"currency" : "EUR",
	"user" : {
		"first_name" : "Bob",
		"last_name" : "Hoskins",
		"gender" : "M",
		"email" : "bob@hoskins.com",
		"address" : "Black 12/4",
		"city" : "London",
		"zip" : 12890,
		"country_code" : "GB",
		"birthday" : "1985-05-01"
	}
}
```
Headers:
* Content-Type = application/json

Additional data should be provided with attributes key. Example JSON body:

```json
{
	"provider" : "paypal",
	"type" : "deposit",
	"amount" : 100,
	"currency" : "EUR",
	"user" : {
		"first_name" : "Bob",
		"last_name" : "Hoskins",
		"gender" : "M",
		"email" : "bob@hoskins.com",
		"address" : "Black 12/4",
		"city" : "London",
		"zip" : 12890,
		"country_code" : "GB",
		"birthday" : "1985-05-01"
	},
	"attributes" : {
		"username" :"user",
		"password" : "very_stringPASSWD123"
	}
}
```


###### Response
```json
{
	"provider" : "paypal",
	"type" : "deposit",
	"amount" : 100,
	"currency" : "EUR",
	"user" : {
		"first_name" : "Bob",
		"last_name" : "Hoskins",
		"gender" : "M",
		"email" : "bob@hoskins.com",
		"address" : "Black 12/4",
		"city" : "London",
		"zip" : 12890,
		"country_code" : "GB",
		"birthday" : "1985-05-01"
	}
}
```

#### Get transactions

Gets all available transaction, with pagination abilities

###### Request 
``` GET 127.0.0.1:8181/api/transactions```

Query filters avaiable:
* filters (```GET http://127.0.0.1:8181/api/transactions?filters[status]=error```)
* limit (```GET http://127.0.0.1:8181/api/transactions?limit=1```)
* page (```GET http://127.0.0.1:8181/api/transactions?page=2```)
* order (```GET http://127.0.0.1:8181/api/transactions?order[id]=desc```)

###### Response
```json
{
    "pagination": {
        "limit": 10,
        "page": 0,
        "order": {
            "id": "ASC"
        }
    },
    "data": [
        {
            "id": 1,
            "user": 1,
            "provider": "paypal",
            "type": "deposit",
            "start_time": "2018-10-19 15:33:14",
            "end_time": "2018-10-19 15:33:16",
            "status": "success",
            "amount": 100,
            "currency": "EUR",
            "attributes": []
        },
        {
            "id": 2,
            "user": 1,
            "provider": "paypal",
            "type": "deposit",
            "start_time": "2018-10-19 15:44:00",
            "end_time": "2018-10-19 15:44:02",
            "status": "success",
            "amount": 100,
            "currency": "EUR",
            "attributes": []
        },
        {
            "id": 3,
            "user": 1,
            "provider": "paypal",
            "type": "deposit",
            "start_time": "2018-10-19 15:44:18",
            "end_time": "2018-10-19 15:44:20",
            "status": "error",
            "amount": 100,
            "currency": "EUR",
            "attributes": []
        },
        {
            "id": 4,
            "user": 1,
            "provider": "paypal",
            "type": "deposit",
            "start_time": "2018-10-19 15:45:54",
            "end_time": "2018-10-19 15:45:56",
            "status": "cancelled",
            "amount": 100,
            "currency": "EUR",
            "attributes": []
        }
    ]
}
```
#### Get transaction details

Gets details of specific transaction.

###### Request 
``` GET 127.0.0.1:8181/api/transactions/1```

###### Response
```json
{
    "id": 1,
    "user": 1,
    "provider": "paypal",
    "type": "deposit",
    "start_time": "2018-10-19 15:33:14",
    "end_time": "2018-10-19 15:33:16",
    "status": "success",
    "amount": 100,
    "currency": "EUR",
    "attributes": []
}
```

#### Get user details

User is unique by email, and providing the same email updates existing user instead of creating 

###### Request 
``` GET 127.0.0.1:8181/api/transactions/1```

###### Response
```json
{
    "id": 1,
    "first_name": "Bob",
    "last_name": "Hoskins",
    "gender": "M",
    "email": "bob@hoskins.com",
    "address": "Black 12/4",
    "city": "London",
    "state": "",
    "zip": "12890",
    "country_code": "GB",
    "birthday": "1985-05-01"
}
```
#### Available transaction statuses
* success (occurs randomly)
* failed (occurs randomly)
* error (occurs randomly)
* cancelled (occurs always when additional data is not provided)