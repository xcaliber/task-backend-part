<?php
/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Action\HomePageAction::class, 'home');
 * $app->post('/album', App\Action\AlbumCreateAction::class, 'album.create');
 * $app->put('/album/:id', App\Action\AlbumUpdateAction::class, 'album.put');
 * $app->patch('/album/:id', App\Action\AlbumUpdateAction::class, 'album.patch');
 * $app->delete('/album/:id', App\Action\AlbumDeleteAction::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Action\ContactAction::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */

/** @var \Zend\Expressive\Application $app */

$app->get('/api/ping', Api\Action\PingAction::class, 'api.ping');
$app->get('/api/methods', Api\Action\GetPaymentMethods::class, 'api.methods');

$app->post('/api/transactions', Api\Action\CreateTransactionAction::class, 'api.create-transaction');

$app->get('/api/transactions', Api\Action\GetTransactionsAction::class, 'api.get-transactions');
$app->get('/api/transactions/{id}', Api\Action\GetTransactionAction::class, 'api.get-transaction');
$app->get('/api/users/{id}', Api\Action\GetUserAction::class, 'api.get-users');
