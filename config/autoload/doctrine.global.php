<?php
return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driver_class' => \Doctrine\DBAL\Driver\PDOSqlite\Driver::class,
                'params' => [
                    'driver' => 'pdo_sqlite',
                    'path' => 'data/db/local.sqlite',
                    'user' => 'root',
                    'charset' => 'UTF8',
                ],
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'metadata_cache' => 'array',
                'result_cache' => 'array',
                'query_cache' => 'array',
                'generate_proxies' => false
            ],
        ],
        'driver' => [
            'orm_default' => [
                'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                'drivers' => [
                    'Api\\Entity' => 'proxy_entities',
                ],
            ],
            'proxy_entities' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    realpath(__DIR__ . '/../../src/Api/Entity')
                ],
            ],
        ],
    ],
];