<?php

return [
    'input_filters' => array(
        'abstract_factories' => array(
            \Zend\InputFilter\InputFilterAbstractServiceFactory::class
        ),
    ),
    'input_filter_specs' => [
        'Api\\Validator\\Transaction' => [
            [
                'name' => 'provider',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Zend\Validator\InArray::class,
                        'options' => [
                            'haystack' => ['paypal', 'cashlib', 'visa', 'entercash'],
                            'messageTemplates' => [
                                'notInArray' => 'Unsupported provider. Known providers are: ' . implode(', ', ['paypal', 'cashlib', 'visa', 'entercash']),
                            ],
                        ],
                    ],
                ],
                'description' => 'Provider name',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ],
            [
                'type' => Zend\InputFilter\InputFilter::class,
                'name' => 'user',
                [
                    'name' => 'first_name',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => []
                ],
                [
                    'name' => 'last_name',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => []
                ],
                [
                    'name' => 'gender',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => []
                ],
                [
                    'name' => 'email',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => [
                        [
                            'name' => \Zend\Validator\StringLength::class,
                            'options' => [
                                'min' => 1,
                                'max' => 320,
                            ],
                        ]
                    ]
                ],
                [
                    'name' => 'address',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => []
                ],
                [
                    'name' => 'city',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => []
                ],
                [
                    'name' => 'state',
                    'required' => false,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => [
                        [
                            'name' => \Zend\Validator\StringLength::class,
                            'options' => [
                                'min' => 1,
                                'max' => 45,
                            ],
                        ],
                    ]
                ],
                [
                    'name' => 'zip',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => []
                ],
                [
                    'name' => 'country_code',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => [
                        [
                            'name' => \Zend\Validator\StringLength::class,
                            'options' => [
                                'min' => 2,
                                'max' => 2,
                            ]
                        ]
                    ]
                ],
                [
                    'name' => 'birthday',
                    'required' => true,
                    'filters' => [
                        [
                            'name' => \Zend\Filter\StringTrim::class
                        ]
                    ],
                    'validators' => [

                    ]
                ],
            ],
            [
                'name' => 'type',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Zend\Validator\InArray::class,
                        'options' => [
                            'haystack' => ['deposit', 'withdraw'],
                            'messageTemplates' => [
                                'notInArray' => 'Unsupported type. Known types are: deposit, withdraw'
                            ],
                        ],
                    ],
                ],
                'description' => 'Type of operation (deposit or withdrawal)',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ],
            [
                'name' => 'amount',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Zend\Validator\GreaterThan::class,
                        'options' => [
                            'min' => 0
                        ],
                    ],
                ],
                'description' => 'An amount',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ],
            [
                'name' => 'currency',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => ['min' => 3, 'max' => 3]
                    ]
                ],
                'description' => '3-letter currency code',
                'allow_empty' => false,
                'continue_if_empty' => false,
            ],
            [
                'name' => 'attributes',
                'required' => false,
                'continue_if_empty' => true,
            ],
        ],
    ]
];
